import Foundation

public class Factory{
    public init(){}
    public func getTransport(type:String)->Transport?{
        switch type{
        case "bus":
            return Bus()
        case "car":
            return Car()
        case "motorcycle":
            return Motorcycle()
        default:
            return nil
        }
    }
}
