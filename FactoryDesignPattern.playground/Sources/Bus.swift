import Foundation
public class Bus:Transport{
    public var isOn: Bool=false
    
    public init(){}
    
    public func turnOn() {
        print("Bus on")
        isOn=true
    }
    
    public func run() {
        if(isOn){
            print("Bus running")
        }else{
            print("The bus is off")
        }
    }
    
    public func turnOff() {
        print("Bus off")
        isOn=false
    }
    
    
}

