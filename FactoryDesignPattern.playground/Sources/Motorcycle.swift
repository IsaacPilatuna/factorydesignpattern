import Foundation

public class Motorcycle:Transport{
    public var isOn: Bool=false
    
    public init(){}
    
    public func turnOn() {
        print("Motorcycle on")
        isOn=true
    }
    
    public func run() {
        if(isOn){
            print("Motorcycle running")
        }else{
            print("The motorcycle is off")
        }
    }
    
    public func turnOff() {
        print("Motorcycle off")
        isOn=false
    }
    
    
}
