import Foundation

public class Car:Transport{
    public var isOn: Bool=false
    public init(){}
    
    public func turnOn() {
        print("Car on")
        isOn=true
    }
    
    public func run() {
        if(isOn){
            print("Car running")
        }else{
            print("The car is off")
        }
    }
    
    public func turnOff() {
        print("Car off")
        isOn=false
    }
    
    
}
