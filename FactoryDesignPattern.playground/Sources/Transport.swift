import Foundation

public protocol Transport {
    var isOn:Bool{get set}
    func turnOn()
    func run()
    func turnOff()
}
