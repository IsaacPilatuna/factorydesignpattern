import UIKit


let factory = Factory()
let transport = factory.getTransport(type:"bus")
transport?.turnOn()
transport?.run()
transport?.turnOff()


